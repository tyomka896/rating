FROM node:18-alpine

WORKDIR /var/node/app

COPY package*.json ./

RUN npm ci --omit=dev
RUN npm i pm2 -g

COPY --chown=node:node . .

USER node

CMD [ "pm2-runtime", "src/index.js" ]
