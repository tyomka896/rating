# Рейтингование участников СМЭВ

## Содержание

 - [Установка](#установка)
   - [Запуск через PM2](#запуск-через-pm2)
   - [Запуск через Docker](#запуск-через-docker)

## Установка

Скачать репозиторий

```bash
git clone https://gitlab.com/aesu/aesu-app.git rating
```

Перейти в директорию проекта


```bash
cd rating
```

Создать файл с переменными

```bash
cp .env.example .env
```

Открыть файл .env любым редактором, заполнить токен `BOT_TOKEN`.

> В качестве хранилища используется _SQLite_, пример структуры БД находится в _./src/storage/app/db.sql_. Чтобы создать файл БД потребуется наличие пакета _sqlite3_.

Для создания БД SQlite из корня проекта выполнить команды

```bash
touch ./src/storage/app/db.sqlite
sqlite3 ./src/storage/app/db.sqlite < ./src/storage/app/db.sql
```

### Запуск через PM2

Для запуска бота через _PM2_ потребуется наличие _NodeJS v16+_, для установки _NodeJS_ и _NPM_ можно воспользоваться документацией репозитория [nodesource/distributions](https://github.com/nodesource/distributions/blob/master/README.md).

Установить менеджер процессов _PM2_

```bash
npm install pm2 -g
```

Запустить проект из корня

```bash
npm run prod
```

### Запуск через Docker

Установить [Docker](https://docs.docker.com/engine/install/), [Docker Compose](https://docs.docker.com/compose/install/linux/#install-using-the-repository) и запустить проект

```bash
docker-compose up -d --build
```
