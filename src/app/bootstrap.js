/**
 * Bootstrapping the Telegram bot
 */
import './http/connection.js'
import { stage } from './stages/index.js'

/** Middleware */
const middleware = [
    'shutDown',
    'signIn',
    'session',
    'lastVisit',
]

/** Commands */
const commands = [
    'start',
    'upload',
    'create',
    'catalog',
    'format',
    'help',
    'cancel',
]

/**
 * Bootstrapping the bot
 */
export async function Bootstrap(bot) {
    /** Setting basic middleware */
    for (let _middleware of middleware) {
        const module = await import(`./http/middleware/${_middleware}.js`)

        bot.use(module.default)
    }

    /** Setting stage */
    bot.use(stage.middleware())

    /** Setting commands to bot */
    for (let command of commands) {
        const module = await import(`./commands/${command}.js`)

        bot.command(command, module.default)
    }

    bot.hears(/^\/.*/, ctx =>
        ctx.reply(
            'Не известная команда.\n' +
            'Отправь /help для вывода полного списка команд.'
        )
    )

    bot.catch(error => console.error(error))

    return bot
}
