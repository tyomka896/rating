/**
 * Cancel last operation
 */
import { isInput, isFile, onInput, aFile, onFile } from '#scene'

export default async function(ctx) {
    if (!isInput(ctx) && !isFile(ctx)) {
        return ctx.reply('Нет активной операции для отмены.')
    }

    onInput(ctx)

    aFile(ctx)
    onFile(ctx)

    return ctx.reply('Последняя операция была отменена.')
}
