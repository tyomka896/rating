/**
 * Update catalog
 */
import { User } from '#models/user.js'

export default async (ctx, next) => {
    const auth = await User.findByPk(ctx.from.id)

    if (!auth.admin()) return next()

    return ctx.scene.enter('main', { act: 'catalog' } )
}
