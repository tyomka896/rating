/**
 * Create report
 */
export default async function(ctx) {
    return ctx.scene.enter('main', { act: 'create' } )
}
