/**
 * Send format of uploading documents
 */
import { SPEED_HEADERS, ACCESS_HEADERS, QUALITY_HEADERS } from '#utils/report.js'

export default async (ctx) => {
    const speedHeaders = Object.keys(SPEED_HEADERS)
        .map(elem => `      - <i>${elem}</i>\n`)
        .join('')
    const accessHeaders = Object.keys(ACCESS_HEADERS)
        .map(elem => `      - <i>${elem}</i>\n`)
        .join('')
    const qualityHeaders = Object.keys(QUALITY_HEADERS)
        .map(elem => `      - <i>${elem}</i>\n`)
        .join('')

    return ctx.reply(
        '<i>Формат загружаемых файлов</i>\n\n' +
        '<b>Для всех</b>\n' +
        '- Формат <i>.csv</i> или <i>.xlsx</i>\n' +
        '- Разделитель для <i>.csv</i> - запятая\n' +
        '- Порядок заголовков не важен\n\n' +
        '<b>Загружаемые документы</b>\n' +
        '<u>Скорость обмена</u>:\n' +
        '   Название файла содержит:\n' +
        '      - <i>soe-smev3</i> или <i>soe-smev4</i>\n' +
        '   Заголовки таблицы:\n' + speedHeaders +
        '<u>Уровень доступности</u>:\n' +
        '   Название файла содержит:\n' +
        '      - <i>al-smev3</i> или <i>al-smev4</i>\n' +
        '   Заголовки таблицы:\n' + accessHeaders +
        '<u>Показатель качества</u>:\n' +
        '   Название файла содержит:\n' +
        '      - <i>Качество</i> или <i>ql</i>\n' +
        '   Заголовки таблицы:\n' + qualityHeaders,
        { parse_mode: 'HTML' }
    )
}
