/**
 * Help command
 */
import { User } from '#models/user.js'

export default async function(ctx) {
    const auth = await User.findByPk(ctx.from.id)

    const rows = [
        'Расчет оценки и формирование отчета рейтингов Участников Взаимодействия.\n\n',
        '*Основные команды*\n',
        '/upload - загрузить документы для отчета\n',
        '/create - сформировать итоговый отчет\n',
        '/format - формат загружаемых документов\n',
        '/cancel - отмена последнего действия\n',
    ]

    if (auth.admin()) {
        rows.push('\n*Дополнительно*\n')
        rows.push('/catalog - обновить справочник организаций\n')
    }

    return ctx.reply(rows.join(''), { parse_mode: 'Markdown' })
}
