/**
 * Start
*/
import help from './help.js'

export default async function(ctx) {
    return help(ctx)
}
