/**
 * Create report
 */
export default async (ctx) => {
    return ctx.scene.enter('main', { act: 'upload' } )
}
