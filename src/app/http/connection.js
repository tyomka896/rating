/**
 * Setting sequelize connection
 */
import path from 'path'
import { Sequelize } from 'sequelize'

export const sequelize = new Sequelize({
    dialect: 'sqlite',
    storage: `${path.resolve()}/src/storage/app/db.sqlite`,
    logging: env.DB_LOGGING ? console.log : false,
    logQueryParameters: env.DB_LOGGING,
    timezone: '+00:00',
})
