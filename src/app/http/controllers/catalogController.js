/**
 * Catalog controller
 */
import fs from 'fs'
import { exportHeaders, readData } from '#utils/csv.js'
import { Catalog } from '#models/catalog.js'

/** Catalog headers mapping */
export const CATALOG_HEADERS = {
    'org_name': 'org_name',
    'ogrn': 'ogrn',
    'type_owner': 'owner',
    'mnemonic': 'mnemonic',
}

export async function resetCatalog(filePath) {
    if (!fs.existsSync(filePath)) {
        throw Error('CSV-file does not found')
    }

    const result = {
        uploaded: 0,
        invalid_ogrn: 0,
    }

    const headers = Object.keys(CATALOG_HEADERS)
    const fileHeaders = await exportHeaders(filePath)

    if (!headers.every(elem => fileHeaders.includes(elem))) {
        result.message = '<b>Необходимые заголовки</b>:\n' +
            headers.map(elem => `- <i>${elem}</i>\n`).join('')

        return result
    }

    await Catalog.destroy({ truncate: true })

    let rowsData = []

    try {
        await readData(filePath, CATALOG_HEADERS, async line => {
            if (/E\+/.test(line.ogrn || '') || !parseInt(line.ogrn)) {
                result.invalid_ogrn++
                return
            }

            rowsData.push({
                org_name: line.org_name.toUpperCase(),
                ogrn: line.ogrn,
                owner: line.owner,
                mnemonic: line.mnemonic,
            })

            if (rowsData.length >= 3000) {
                await Catalog.bulkCreate(rowsData)

                rowsData = []
            }

            result.uploaded++
        })

        if (rowsData.length > 0) {
            await Catalog.bulkCreate(rowsData)
        }
    } catch (error) {
        result.message = `<b>Ошибка:</b> <i>${error.message}</i>`
    }

    return result
}
