/**
 * User last visit
 */
import { sequelize } from '#connection'
import dayjs from 'dayjs'
import utc from 'dayjs/plugin/utc.js'

dayjs.extend(utc)

export default async function(ctx, next) {
    await sequelize
        .query('UPDATE "user" SET "last_visit"=? WHERE "id"=?', {
            replacements: [
                dayjs.utc().format('YYYY-MM-DD HH:mm:ss.SSS Z'),
                ctx.from.id,
            ]
        })

    return next()
}
