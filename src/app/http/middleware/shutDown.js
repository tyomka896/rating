/**
 * Shut down the app
 */
export default async function(ctx, next) {
    if (env.BOT_SHUTDOWN) {
        ctx.reply('Бот временно не работает.\nПросим прощения 🙏')

        return null
    }

    return next()
}
