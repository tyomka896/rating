/**
 * Sign in the user
 */
import dayjs from 'dayjs'
import utc from 'dayjs/plugin/utc.js'
import { User } from '#models/user.js'

dayjs.extend(utc)

export default async function(ctx, next) {
    let auth = await User.findByPk(ctx.from.id)

    if (!auth) {
        auth = await User.create({
            id: ctx.from.id,
            username: ctx.from.username,
            first_name: ctx.from.first_name,
            last_name: ctx.from.last_name,
            last_visit: dayjs.utc().format('YYYY-MM-DD HH:mm:ss.SSS Z'),
        })

        console.log(`New user '${auth.fullName()}' (#${auth.id}) was created!`)
    }
    else if (auth.first_name !== ctx.from.first_name ||
        auth.last_name != ctx.from.last_name ||
        auth.username != ctx.from.username) {
        auth.set({
            first_name: ctx.from.first_name,
            last_name: ctx.from.last_name || null,
            username: ctx.from.username || null,
        })

        await auth.save()
    }

    return next()
}
