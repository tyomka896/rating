/**
 * Catalog model
 */
import { Model, DataTypes } from 'sequelize'
import { sequelize } from '#connection'

/**
 * Extending model
 */
export class Catalog extends Model {  }

/**
 * Model structure
 */
Catalog.init({
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    org_name: {
        type: DataTypes.TEXT,
        allowNull: false,
    },
    owner: {
        type: DataTypes.TEXT,
        allowNull: true,
    },
    ogrn: {
        type: DataTypes.TEXT,
        defaultValue: '0000000000000',
        allowNull: false,
    },
    mnemonic: {
        type: DataTypes.TEXT,
        allowNull: false,
    },
    created_at: {
        type: DataTypes.DATE,
        allowNull: false,
    },
}, {
    sequelize,
    tableName: 'catalog',
    timestamps: true,
    createdAt: 'created_at',
    updatedAt: false,
})
