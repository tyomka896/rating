/**
 * User model
 */
import { Model, DataTypes } from 'sequelize'
import { sequelize } from '#connection'
import { Session } from '#models/session.js'

/**
 * Extending model
 */
export class User extends Model {
    /**
     * If the user is admin
     */
    admin() {
        return this.role === 'admin'
    }

    /**
     * Concat last name, first name and login
     */
    fullName() {
        const parts = []

        parts.push(this.last_name || '')
        parts.push(this.first_name || '')
        parts.push(this.username ? `@${this.username}` : '')

        return parts
            .filter(elem => elem)
            .join(' ')
    }
}

/**
 * Model structure
 */
User.init({
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    utc: {
        type: DataTypes.INTEGER,
        allowNull: false,
        defaultValue: 0,
    },
    role: {
        type: DataTypes.TEXT,
        allowNull: false,
        defaultValue: 'user',
    },
    username: {
        type: DataTypes.TEXT,
    },
    first_name: {
        type: DataTypes.TEXT,
        allowNull: false,
    },
    last_name: {
        type: DataTypes.TEXT,
    },
    created_at: {
        type: DataTypes.DATE,
        allowNull: false,
    },
    updated_at: {
        type: DataTypes.DATE,
        allowNull: false,
    },
    last_visit: {
        type: DataTypes.DATE,
        allowNull: false,
    }
}, {
    sequelize,
    tableName: 'user',
    timestamps: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
})

/** User relation with Session as one to many */
User.hasMany(Session, {
    foreignKey: 'user_id',
    as: 'Session'
})
