/**
 * Main catalog scene
 */
import { onFile, aFile } from '#scene'
import { xlsxToCsv } from '#utils/csv.js'
import { download, isUtf8 } from '#utils/helpers.js'
import { resetCatalog } from '#controllers/catalogController.js'

/** Keyboard */
export async function catalogKeyboard(ctx) {
    onFile(ctx, catalogFile)

    return [ 'Загрузите справочник организаций.' ]
}

/** Documents */
export async function catalogFile(ctx) {
    const { document } = ctx.message
    const { file_name, file_id, file_size } = document

    if (!/\.(csv|xlsx)$/.test(file_name)) {
        const extension = file_name.split('.').pop() || ''

        return ctx.reply(
            `Не удалось опознать формат документа <b>.${extension}</b>\n` +
            'Допустимы <i>CSV</i> и <i>XLSX</i> форматы.',
            { parse_mode: 'HTML' }
        )
    }

    if (file_size > 10 ** 6) {
        await ctx.reply(
            '<i>Началось обновление справочника. . .</i>',
            { parse_mode: 'HTML' }
        )
    }

    const path = await download(ctx, file_id)

    if (/\.csv$/.test(file_name)) {
        if (!await isUtf8(path)) {
            await ctx.reply(
                '<i><u>Рекомендуется использовать кодировку UTF-8</u></i>.',
                { parse_mode: 'HTML' }
            )
        }
    }
    else if (/\.xlsx$/.test(file_name)) {
        if (!xlsxToCsv(path)) {
            aFile(ctx, type, { remove: true })

            return ctx.reply('Не удалось преобразовать XLSX-документ.')
        }
    }

    aFile(ctx, 'catalog', { id: file_id, path: path })

    const result = await resetCatalog(path)

    aFile(ctx, 'catalog', { remove: true })

    if (result.message) {
        return ctx.reply(
            'Не удалось загрузить справочник.\n\n' + (result.message || ''),
            { parse_mode: 'HTML' }
        )
    }

    onFile(ctx)

    const rows = []

    if (result.invalid_ogrn) {
        const { uploaded, invalid_ogrn } = result

        rows.push(
            `<i>Успешно загружено ИС ${uploaded}\n` +
            `Найдено ошибок ОГРН в ${invalid_ogrn} из ${uploaded + invalid_ogrn}</i>\n\n`
        )
    }

    rows.push('Справочник организаций успешно обновлен.')

    return ctx.reply(rows.join(''), { parse_mode: 'HTML' })
}
