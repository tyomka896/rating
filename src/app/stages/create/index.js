/**
 * Create final report
 */
import fs from 'fs'
import dayjs from 'dayjs'
import utc from 'dayjs/plugin/utc.js'
import { Markup } from 'telegraf'
import { User } from '#models/user.js'
import { mainScene, onFile, aFile } from '#scene'
import { RatingCompose, FILES_PREFIX } from '#utils/report.js'

dayjs.extend(utc)

/** Methods */
function listOfFiles(files) {
    if (typeof files !== 'object') return {}

    const names = Object.keys(files)

    const hasFiles = names.length !== 0 &&
        names.every(elem => FILES_PREFIX.includes(elem))

    if (!hasFiles) return {}

    return names.reduce((red, elem) => {
        red[elem] = files[elem].path

        return red
    }, {})
}

/** Keyboard */
export async function createKeyboard(ctx) {
    const files = ctx.session.files || {}
    const paths = listOfFiles(files)
    const keys = Object.keys(paths)

    if (keys.length === 0) {
        return [
            'Не добавлено ни одного документа.\n' +
            'Введите /upload чтобы загрузить список файлов.'
        ]
    }

    const rows = [
        'Будет сформирован отчет по:\n\n',
        `${paths.speed3 ? '✅' : '🚫'} Скорость обмена СМЭВ3\n`,
        `${paths.access3 ? '✅' : '🚫'} Уровень доступности СМЭВ3\n`,
        `${paths.speed4 ? '✅' : '🚫'} Скорость обмена СМЭВ4\n`,
        `${paths.access4 ? '✅' : '🚫'} Уровень доступности СМЭВ4\n`,
        `${paths.quality ? '✅' : '🚫'} Показатель качества\n\n`,
        'Нажмите <b>Создать</b> для генерации отчета.',
    ]

    return [
        rows.join(''),
        {
            ...Markup.inlineKeyboard([
                Markup.button.callback('Создать', 'create-generate'),
            ]),
            parse_mode: 'HTML'
        }

    ]
}

/** Actions */
mainScene.action('create-generate', async ctx => {
    const files = ctx.session.files || {}

    const paths = listOfFiles(files)

    if (Object.keys(paths).length === 0) {
        return [
            'Не добавлено ни одного документа.\n' +
            'Введите /upload чтобы загрузить список файлов.'
        ]
    }

    let filePath = null

    try {
        filePath = await new RatingCompose().create(paths)

        ctx.answerCbQuery()

        await ctx.replyWithDocument({
            source: filePath,
            filename: `Отчет (${dayjs.utc().format('DD.MM.YYYY')}).xlsx`
        })

        const auth = await User.findByPk(ctx.from.id)
        console.log(`User '${auth.fullName()}' (#${auth.id}) successfully created a report!`)
    } catch (error) {
        return ctx.reply(
            'Произошла ошибка.\n\n' +
            `<i>${error.message}</i>`,
            { parse_mode: 'HTML' }
        )
    } finally {
        aFile(ctx)
        onFile(ctx)

        if (filePath) fs.unlinkSync(filePath)
    }
})
