/**
 * Main stage
 */
import { Scenes } from 'telegraf'
import { mainScene } from '#scene'
import help from '../commands/help.js'
import { uploadKeyboard } from './upload/index.js'
import { createKeyboard } from './create/index.js'
import { catalogKeyboard } from './catalog/index.js'

/** Actions */
mainScene.enter(async ctx => {
    let text, extra

    switch (ctx.scene.state.act) {
        case 'upload': [ text, extra ] = await uploadKeyboard(ctx); break
        case 'create': [ text, extra ] = await createKeyboard(ctx); break
        case 'catalog': [ text, extra ] = await catalogKeyboard(ctx); break
        default: return help(ctx)
    }

    return ctx.reply(text, extra)
})

/**
 * User's input handler
 */
mainScene.hears(/^[^\/].*/, async ctx => {
    if (ctx.session.input) {
        const { path, name } =  ctx.session.input

        const module = await import(path)

        if (module[name]) {
            return module[name](ctx)
        }
    }

    return ctx.reply('Отправь /help для вывода полного списка команд.')
})

/**
 * Document send handler
 */
mainScene.on('document', async ctx => {
    if (!ctx.session.file) return

    const { path, name } =  ctx.session.file

    const module = await import(path)

    if (module[name]) {
        return module[name](ctx)
    }
})

export const stage = new Scenes.Stage([ mainScene ])
