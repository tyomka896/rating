/**
 * Handle document send
 */
import fs from 'fs'
import { validateCtx } from '#utils/helpers.js'

/**
 * Is file expected
 */
export function isFile(ctx) {
    validateCtx(ctx)

    return Boolean(ctx.session.file)
}

/**
 * Remember uploaded documents by user or
 * if name not defined then remove all downloaded
 *
 * @param {Context} ctx
 * @param {String} name
 * @param {Object} options (id|url|name|path|remove)
 * @return {Boolean}
 */
export function aFile(ctx, name, options) {
    validateCtx(ctx)

    if (!ctx.session) return false

    const files = ctx.session.files || {}

    if (!name) {
        Object.keys(files).forEach(elem => {
            if (!files[elem].path) return

            if (!fs.existsSync(files[elem].path)) return

            fs.unlinkSync(files[elem].path)
        })

        delete ctx.session.files

        return true
    }

    if (options.remove) {
        if (!files[name].path || !fs.existsSync(files[name].path)) {
            return false
        }

        fs.unlinkSync(files[name].path)

        delete ctx.session.files[name]

        return true
    }

    if (files[name] && fs.existsSync(files[name].path)) {
        fs.unlinkSync(files[name].path)
    }

    const _options = {}

    options.id ? _options.id = options.id : null
    options.url ? _options.url = options.url : null
    options.name ? _options.name = options.name : null
    options.path ? _options.path = options.path : null

    ctx.session.files = {
        ...files,
        [name]: {
            ...files[name],
            ..._options,
        }
    }

    return false
}

/**
 * Called module file path
 */
function modulePath() {
    const stackFile = new Error().stack.split('\n')[3]

    const filePath = new RegExp('file:\/+(\/{1}.+[^:0-9]):[0-9]+:').exec(stackFile)

    if (!filePath) return null

    return filePath[1]
}

/**
 * Change document handler
 */
export function onFile(ctx, method) {
    validateCtx(ctx)

    if (!ctx.session) return ctx

    if (!method || (typeof method !== 'string' && typeof method !== 'function')) {
        return delete ctx.session.file
    }

    const path = modulePath()

    if (!path) return ctx

    ctx.session.file = {
        path: path,
        name: typeof method === 'function' ? method.name : method,
    }

    return ctx
}
