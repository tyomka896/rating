/**
 * Upload documents for the report
 */
import { onFile, aFile } from '#scene'
import { download, isUtf8 } from '#utils/helpers.js'
import { exportHeaders, xlsxToCsv } from '#utils/csv.js'
import { SPEED_HEADERS, ACCESS_HEADERS, QUALITY_HEADERS } from '#utils/report.js'

/** Keyboard */
export async function uploadKeyboard(ctx) {
    aFile(ctx)
    onFile(ctx, uploadFiles)

    return [ 'Отправьте документы для отчета по одному.' ]
}

/** Documents */

/**
 * @param {String} name
 * @return {Array} [type, headers]
 */
function defineFile(name) {
    if (!name || typeof name !== 'string') return []

    name = name.toLowerCase()

    if (/(soe-smev3|soe-smev4)/.test(name)) {
        return [
            name.includes('soe-smev3') ? 'speed3' : 'speed4',
            Object.keys(SPEED_HEADERS),
        ]
    }
    else if (/(al-smev3|al-smev4)/.test(name)) {
        return [
            name.includes('al-smev3') ? 'access3' : 'access4',
            Object.keys(ACCESS_HEADERS),
        ]
    }
    else if (/(ql|качеств)/.test(name)) {
        return [ 'quality', Object.keys(QUALITY_HEADERS) ]
    }
    else return []
}

export async function uploadFiles(ctx) {
    const { document } = ctx.message
    const { file_name, file_id } = document

    if (!/\.(csv|xlsx)$/.test(file_name)) {
        const extension = file_name.split('.').pop() || ''

        return ctx.reply(
            `Не удалось опознать формат документа <b>.${extension}</b>\n` +
            'Допустимы <i>CSV</i> и <i>XLSX</i> форматы.',
            { parse_mode: 'HTML' }
        )
    }

    const [ type, headers ] = defineFile(file_name)

    if (!type) {
        return ctx.reply(
            `Не удалось опознать название <i>${file_name}</i>.\n` +
            'Введите /format для большей информации.',
            { parse_mode: 'HTML' }
        )
    }

    const path = await download(ctx, file_id)

    aFile(ctx, type, { id: file_id, path: path })

    if (/\.csv$/.test(file_name)) {
        if (!await isUtf8(path)) {
            await ctx.reply(
                '<i><u>Рекомендуется использовать кодировку UTF-8</u></i>.',
                { parse_mode: 'HTML' }
            )
        }
    }
    else if (/\.xlsx$/.test(file_name)) {
        if (!xlsxToCsv(path)) {
            aFile(ctx, type, { remove: true })

            return ctx.reply('Не удалось преобразовать XLSX-документ.')
        }
    }

    const fileHeaders = await exportHeaders(path)

    if (!headers.every(elem => fileHeaders.includes(elem))) {
        aFile(ctx, type, { remove: true })

        return ctx.reply(
            `В файле <i>${file_name}</i> не верно заданы заголовки.\n` +
            'Введите /format для большей информации.',
            { parse_mode: 'HTML' }
        )
    }

    return ctx.reply(
        `Файл <i>${file_name}</i> загружен успешно.\n` +
        'Отправьте /create чтобы создать отчет.',
        { parse_mode: 'HTML' }
    )
}
