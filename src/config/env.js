/**
 * App configuration
 */
import 'dotenv/config'

global.env = {
    BOT_TOKEN: process.env.BOT_TOKEN,
    BOT_SHUTDOWN: process.env.BOT_SHUTDOWN === 'true'||
        Boolean(parseInt(process.env.BOT_SHUTDOWN)),

    DB_LOGGING: process.env.DB_LOGGING === 'true'||
        Boolean(parseInt(process.env.DB_LOGGING)),
}
