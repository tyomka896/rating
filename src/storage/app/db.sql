--
-- File generated with SQLiteStudio v3.3.3 on ѕт сен 9 20:24:25 2022
--
-- Text encoding used: System
--
PRAGMA foreign_keys = off;
BEGIN TRANSACTION;

-- Table: catalog
DROP TABLE IF EXISTS catalog;

CREATE TABLE catalog (
    id         INTEGER PRIMARY KEY
                       NOT NULL,
    org_name   TEXT    NOT NULL,
    ogrn       TEXT    DEFAULT ('0000000000000') 
                       NOT NULL,
    owner      TEXT,
    mnemonic   TEXT    NOT NULL,
    created_at TEXT    NOT NULL
);


-- Table: session
DROP TABLE IF EXISTS session;

CREATE TABLE session (
    user_id    INTEGER NOT NULL
                       CONSTRAINT session_user_id REFERENCES user (id) ON DELETE CASCADE
                                                                       ON UPDATE CASCADE,
    chat_id    INTEGER NOT NULL,
    value      TEXT    NOT NULL
                       DEFAULT ('{}'),
    created_at TEXT    NOT NULL,
    updated_at TEXT    NOT NULL,
    CONSTRAINT session_user_chat_id PRIMARY KEY (
        user_id,
        chat_id
    )
);


-- Table: user
DROP TABLE IF EXISTS user;

CREATE TABLE user (
    id         INTEGER PRIMARY KEY
                       UNIQUE
                       NOT NULL,
    utc        INTEGER NOT NULL
                       DEFAULT (0),
    role       TEXT    NOT NULL
                       DEFAULT user,
    username   TEXT    UNIQUE,
    first_name TEXT    NOT NULL,
    last_name  TEXT,
    created_at TEXT    NOT NULL,
    updated_at TEXT    NOT NULL,
    last_visit TEXT    NOT NULL
);


COMMIT TRANSACTION;
PRAGMA foreign_keys = on;
