/**
 * Module for csv-files
 */
import fs from 'fs'
import { parse } from 'csv-parse'
import xlsx from 'xlsx'

/** CSV possible delimiters */
const CSV_DELIMITERS = [ ',', ';' ]

/**
 * Export header of the csv-file
 *
 * @param {String} absolute file path
 * @returns {Promise<any>}
 */
export function exportHeaders(filePath) {
    if (!fs.existsSync(filePath)) {
        throw Error('CSV-file does not found')
    }

    const parser = parse({
        encoding: 'utf8',
        delimiter: CSV_DELIMITERS,
        to_line: 1,
    })

    fs.createReadStream(filePath).pipe(parser)

    return new Promise(resolve => {
        parser.on('data', row => resolve(row))
    })
}

/**
 * Parse file line  by line
 *
 * @param {String} absolute file path
 * @param {Function|Object} mapping map for headers
 * @param {Function} callback
 */
export async function readData(filePath, mapping, callback) {
    if (!fs.existsSync(filePath)) {
        throw Error('CSV-file does not found')
    }

    let columns = true

    if (typeof mapping === 'function') {
        callback = mapping
    }
    else if (Array.isArray(mapping)) {
        columns = mapping
    }
    else if (typeof mapping === 'object') {
        columns = header => header.map(elem => mapping[elem] || elem)
    }
    else {
        throw Error('Unknown mapping parameter')
    }

    if (typeof callback !== 'function') {
        throw Error('Callback should be a function')
    }

    const parser = parse({
        delimiter: CSV_DELIMITERS,
        columns: columns,
        encoding: 'utf8',
    })

    fs.createReadStream(filePath).pipe(parser)

    for await (let line of parser) {
        await callback(line)
    }
}

/**
 * Convert XLSX document to CSV
 *
 * @param {String} filePath
 * @param {String} saveAs
 */
export function xlsxToCsv(filePath, saveAs) {
    if (!fs.existsSync(filePath)) {
        throw Error('XLSX-file does not found')
    }

    try {
        const wb = xlsx.readFile(filePath)

        xlsx.writeFile(wb, saveAs || filePath, {
            FS: ',',
            bookType: 'csv',
        })
    } catch (error) {
        return false
    }

    return true
}
