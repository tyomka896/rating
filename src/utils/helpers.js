/**
 * Helper module for basic usage methods
 */
import fs from 'fs'
import path from 'path'
import readline from 'readline'
import axios from 'axios'
import dayjs from 'dayjs'
import utc from 'dayjs/plugin/utc.js'

dayjs.extend(utc)

/**
 * Download file from telegram's storage
 *
 * @param {Context} ctx
 * @param {String} fileId
 * @returns file path in local storage
 */
export async function download(ctx, fileId) {
    validateCtx(ctx)

    if (!ctx.message.document) {
        return null
    }

    if (!fileId) {
        fileId = ctx.message.document.file_id
    }

    const filePath = path.resolve(
        './src/storage/data/',
        dayjs.utc().format('YYYYMMDDHHmmss.SSS')
    )

    try {
        const fileData = await ctx.telegram.getFileLink(fileId)

        const fileResponse = await axios.get(fileData.href, { responseType: 'stream' })

        const writeStream = fs.createWriteStream(filePath)

        fileResponse.data.pipe(writeStream)

        return new Promise(resolve => {
            writeStream.on('finish', () => {
                writeStream.close()
                resolve(filePath)
            })
        })
    } catch (error) {
        console.error(error.message)

        return null
    }
}

/**
 * Check if file has UTF-8 encoding
 */
export async function isUtf8(filePath) {
    if (!fs.existsSync(filePath)) {
        throw Error('File does not found')
    }

    const stream = fs.createReadStream(filePath)
    const reader = readline.createInterface({ input: stream })

    let count = 3
    let result = true

    for await (const line of reader) {
        if (!checkIsUtf8(line)) {
            result = false
            break
        }

        if (--count <= 0) break
    }

    reader.close()
    stream.close()

    return result
}

function checkIsUtf8(line) {
    for (let lt of line) {
        if (lt === '�') {
            return false
        }
    }
    return true
}

/**
 * Context object validation
 */
export function validateCtx(ctx) {
    if (ctx && ctx.constructor?.name === 'Context') return true

    throw Error('Cannot read object ctx as Context, should be defined')
}
