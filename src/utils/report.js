/**
 * Module for generating main report
 */
import fs from 'fs'
import path from 'path'
import excel from 'excel4node'
import { Op } from 'sequelize'
import dayjs from 'dayjs'
import utc from 'dayjs/plugin/utc.js'
import { readData } from '#utils/csv.js'
import { Catalog } from '#models/catalog.js'

dayjs.extend(utc)

/** All passed file prefixes that may be handled */
export const FILES_PREFIX = [
    'speed3',
    'speed4',
    'access3',
    'access4',
    'quality',
]

/** Do not include this mnemonics */
const EXCLUDE_MNEMONIC = [ 
    'emuOLD', 
    'MNSV56', 
    'smev3',
]

/** Speed of exchange headers mapping */
export const SPEED_HEADERS = {
    'Мнемоника': 'mnemonic',
    'Скорость обмена (мс)': 'speed',
}

/** Availability level headers mapping */
export const ACCESS_HEADERS = {
    'Мнемоника': 'mnemonic',
    'U уровень доступности %': 'access',
}

/** Quality level headers mapping */
export const QUALITY_HEADERS = {
    'ОГРН': 'ogrn',
    'Итого': 'quality',
}

/**
 * Speed of execution score
 *
 * @param {Number} value - time in seconds
 * @return {Number} rate by k1 coefficient
 */
function speedOfExecution(value) {
    if (!value && value !== 0) return null
    else if (value <= 2) return 5
    else if (value > 2 && value <= 60 ) return 4
    else if (value > 60 && value <= 43200) return 3
    else if (value > 43200 && value <= 172800 ) return 2
    else return 1
}

/**
 * Access of exchange score
 *
 * @param {Number} value - value in percent
 * @return {Number} rate by k2 coefficient
 */
function accessOfExchange(value) {
    if (!value && value !== 0) return null
    else if (value >= 99) return 5
    else if (value >= 95 && value < 99 ) return 4
    else if (value >= 85 && value < 95) return 3
    else if (value >= 70 && value < 85) return 2
    else return 1
}

/**
 * Level of quality score
 *
 * @param {Number} value - value in percent
 * @return {Number} rate by k3 coefficient
 */
function levelOfQuality(value) {
    if (!value && value !== 0) return null
    else if (value >= 97) return 5
    else if (value >= 90 && value < 97) return 4
    else if (value >= 75 && value < 90) return 3
    else if (value >= 50 && value < 75) return 2
    else return 1
}

export class RatingCompose {
    /** Excel Workbook */
    #workbook = null

    /** Main Excel sheet */
    #mainsheet = null

    /** Basic styles for cells */
    #styles = null

    /** All validated files */
    #files = {}

    /**
     * Main method to create a file report
     *
     * @param {Array} files { prefix_name: 'absolute-file-path' }
     * @return {Promise} file path of saved excel document
     */
    async create(files, name) {
        if (!files || Object.keys(files).length === 0) {
            throw Error('Files parameter is incorrect')
        }

        if (this.#parseFiles(files) === 0) {
            throw Error('No valid files passed')
        }

        const catalogs = await this.#selectCatalog()

        this.#defineWorkbook().#defineStyles().#defineHeaders()

        let mainRow = 2
        const catalogKeys = Object.keys(catalogs)

        for (let catalog of catalogKeys) {
            let _mainRow = mainRow

            let allSpeed = []
            let allAccess = []

            const { org_name, ogrn, owner, mnemonics } = catalogs[catalog]

            // Field: 'Владелец'
            this.#addCell(mainRow, 1, org_name)
            // Field: 'ОГРН'
            this.#addCell(mainRow, 2, ogrn)
            // Field: 'Тип'
            this.#addCell(mainRow, 3, owner)

            mnemonics.forEach((elem, index, { length }) => {
                const access = elem.access >= 0 ? elem.access : null
                const accessScore = accessOfExchange(access) || null

                // Define speed only when access is set
                const speed = access && elem.speed / 1000 || null
                let speedScore = access && speedOfExecution(speed) || null

                if (!speed && access >= 0) speedScore = 5

                speedScore ? allSpeed.push(speedScore) : void 0
                accessScore ? allAccess.push(accessScore) : void 0

                // If not speed or access specified, then skip the row
                if (!speed && speed !== 0 && !access && access !== 0) return

                // Field: 'Мнемоника'
                this.#addCell(mainRow, 4, elem.mnemonic, null, 'middle')
                // Field: 'Скорость (сек.)'
                this.#addCell(mainRow, 5, speed, '-', 'float2')
                // Field: 'Оценка скорости ИС'
                this.#addCell(mainRow, 6, speedScore, '-', 'middle')
                // Field: 'Уровень доступности (%)'
                this.#addCell(mainRow, 8, access && access / 100, '-', 'percent')
                // Field: 'Оценка доступности ИС'
                this.#addCell(mainRow, 9, accessScore, '-', 'middle')

                if (index != length - 1) mainRow++
            })

            allSpeed = allSpeed.reduce((red, elem) => red + elem, 0) / allSpeed.length || 0
            allAccess = allAccess.reduce((red, elem) => red + elem, 0) / allAccess.length || 0

            // Field: 'Оценка скорости УВ'
            this.#addCell(_mainRow, 7, allSpeed, '-', [ 'middle', 'float2' ])
            // Field: 'Оценка доступности УВ'
            this.#addCell(_mainRow, 10, allAccess, '-', [ 'middle', 'float2' ])

            const quality = catalogs[catalog].quality
            const qualityScore = levelOfQuality(quality) || 5

            // Insert quality data if there is no speed or access info
            if (mnemonics.length === 0) {
                // Field: 'Владелец'
                this.#addCell(_mainRow, 1, org_name)
                // Field: 'ОГРН'
                this.#addCell(_mainRow, 2, ogrn)
                // Field: 'Тип'
                this.#addCell(_mainRow, 3, owner)
                // Field: 'Оценка скорости УВ'
                this.#addCell(_mainRow, 7, 5, '-', [ 'middle', 'float2' ])
                // Field: 'Оценка доступности УВ'
                this.#addCell(_mainRow, 10, 5, '-', [ 'middle', 'float2' ])
            }

            // Field: 'Качество сопровождения УВ (%)'
            this.#addCell(_mainRow, 11, quality && quality / 100, '-', 'percent')
            // Field: 'Оценка сопровождения УВ'
            this.#addCell(_mainRow, 12, qualityScore, '-', [ 'middle' ])

            // Field: 'Итоговая оценка участника'
            this.#addCell(
                _mainRow, 13,
                `=(G${_mainRow} + J${_mainRow} + L${_mainRow}) / 15 * 1`, null,
                [ 'middle', 'float2', 'finalScore' ]
            )

            mainRow++
        }

        const filePath = path.resolve(
            './src/storage/data',
            name || dayjs.utc().format('YYYYMMDDHHmmss-r.SSS')
        )

        return new Promise(resolve =>
            this.#workbook.write(filePath, error => {
                if (error) throw error

                resolve(filePath)
            })
        )
    }

    /**
     * Select all needed catalog objects
     *
     * @returns
     */
    async #selectCatalog() {
        const commonData = {}

        /** Callbacks */
        const _callbackSpeedData = (red, elem) => {
            red[elem.mnemonic] = {
                speed: parseFloat(elem.speed.replace(',', '.')),
                amount: parseInt(elem.amount),
                mnemonic: elem.mnemonic,
            }
        }

        const _callbackAccessData = (red, elem) => {
            red[elem.mnemonic] = {
                access: parseFloat(elem.access.replace(',', '.')),
                mnemonic: elem.mnemonic,
            }
        }

        /** Gathering data */
        const speedData = {
            ...await this.#readFileData(
                {
                    path: this.#files.speed3,
                    headers: SPEED_HEADERS,
                    type: 'Скорость обмена СМЭВ3',
                },
                _callbackSpeedData
            ),
            ...await this.#readFileData(
                {
                    path: this.#files.speed4,
                    headers: SPEED_HEADERS,
                    type: 'Скорость обмена СМЭВ4',
                },
                _callbackSpeedData
            ),
        }

        const accessData = {
            ...await this.#readFileData(
                {
                    path: this.#files.access3,
                    headers: ACCESS_HEADERS,
                    type: 'Уровень доступности СМЭВ3',
                },
                _callbackAccessData
            ),
            ...await this.#readFileData(
                {
                    path: this.#files.access4,
                    headers: ACCESS_HEADERS,
                    type: 'Уровень доступности СМЭВ4',
                },
                _callbackAccessData
            ),
        }

        const qualityData = await this.#readFileData({
            path: this.#files.quality,
            headers: QUALITY_HEADERS,
            type: 'Показатель качества',
        }, (red, elem) => {
            red[elem.ogrn] = elem.quality && parseFloat(elem.quality.replace('%',''))
        })

        // Reformat common mnemonics of speed and access
        const mnemonics = Array.from(
            new Set([
                ...Object.keys(speedData),
                ...Object.keys(accessData),
            ])
        )
            .filter(elem => ! EXCLUDE_MNEMONIC.includes(elem))

        for (let mnemonic of mnemonics) {
            const _speed = speedData[mnemonic],
                _access = accessData[mnemonic]

            commonData[mnemonic] = {
                mnemonic: mnemonic,
                speed: _speed ? _speed.speed : null,
                access: _access ? _access.access : null
            }
        }

        // Select all needed catalog rows and map it
        const catalog = await Catalog.findAll({
            where: {
                [Op.and]: [
                    { mnemonic: { [Op.in]: mnemonics } },
                    { owner: { [Op.in]: [ 'ФОИВ' ] } },
                ]
            },
            order: [ 'org_name' ],
        })

        // Select all organizations without speed and access
        let ogrns = catalog.map(elem => elem.ogrn)
        ogrns = Object.keys(qualityData).filter(elem => ogrns.indexOf(elem) < 0)

        const remainedQuality = await Catalog.findAll({
            where: {
                [Op.and]: [
                    { ogrn: { [Op.in]: ogrns } },
                    { owner: { [Op.in]: [ 'ФОИВ' ] } },
                ]
            },
            group: 'ogrn',
            order: [ 'org_name' ],
        })

        return catalog.concat(remainedQuality).reduce((red, elem) => {
            if (!red[elem.ogrn]) {
                red[elem.ogrn] = {
                    org_name: elem.org_name,
                    ogrn: elem.ogrn,
                    owner: elem.owner,
                    quality: qualityData[elem.ogrn],
                    mnemonics: [],
                }
            }

            if (commonData[elem.mnemonic]) {
                red[elem.ogrn].mnemonics.push(commonData[elem.mnemonic])
            }

            return red
        }, {})
    }

    /**
     * Read all data from a file
     *
     * @param {Object} options (path|type|headers)
     * @param {Function} callback handle every row
     * @returns data from file
     */
    async #readFileData(options, callback) {
        const { path, type, headers } = options

        if (!fs.existsSync(path)) return {}

        const obj = {}

        try {
            await readData(path, headers, row => callback(obj, row))
        } catch (error) {
            throw Error((type ? `Файл: ${type}\n` : '') + `Ошибка: ${error.message}`)
        }

        return obj
    }

    /**
     * Add a cell to a mainsheet
     *
     * @param {Number} row
     * @param {Number} col
     * @param {any} val
     * @param {any} def default
     * @param {Style|Array} styles
     * @returns {Cell}
     */
    #addCell(row, col, val, def, styles) {
        const _cell = this.#mainsheet.cell(row, col)

        if (typeof val === 'string') {
            if (val[0] === '=') {
                _cell.formula(val)
            } else {
                _cell.string(val)
            }
        }
        else if (typeof val === 'number' && !Number.isNaN(val)) {
            _cell.number(val)
        }
        else {
            styles = [ 'middle' ]

            _cell.string(def || '')
        }

        if (styles) {
            if (!Array.isArray(styles)) {
                styles = [ styles ]
            }

            styles.forEach(elem =>
                this.#styles[elem] &&
                _cell.style(this.#styles[elem])
            )
        }

        return _cell
    }

    /**
     * Validate passed files paths
     * (already should be downloaded)
     *
     * @param {Array} files
     * @return {Number} amount of validated files
     */
    #parseFiles(files) {
        this.#files = {}

        FILES_PREFIX.forEach(elem => {
            if (!(elem in files)) return

            if (!fs.existsSync(files[elem])) return

            this.#files[elem] = files[elem]
        })

        return Object.keys(this.#files).length
    }

    /**
     * Define headers of main sheet
     */
    #defineHeaders() {
        this.#mainsheet.row(1).setHeight(35).freeze().filter();

        [
            { width: 18, name: 'Владелец' },                      // 1
            { width: 18, name: 'ОГРН' },                          // 2
            { width: 16, name: 'Тип' },                           // 3
            { width: 18, name: 'Мнемоника' },                     // 4
            { width: 14, name: 'Скорость (сек.)' },               // 5
            { width: 16, name: 'Оценка скорости ИС' },            // 6
            { width: 16, name: 'Оценка скорости УВ' },            // 7
            { width: 20, name: 'Уровень доступности (%)' },       // 8
            { width: 20, name: 'Оценка доступности ИС' },         // 9
            { width: 20, name: 'Оценка доступности УВ' },         // 10
            { width: 24, name: 'Качество сопровождения УВ (%)' }, // 11
            { width: 22, name: 'Оценка сопровождения УВ' },       // 12
            { width: 20, name: 'Итоговая оценка участника' },     // 13
        ].forEach((elem, pos) =>
            this.#mainsheet.column(pos + 1).setWidth(elem.width)
                .ws.cell(1, pos + 1).string(elem.name).style(this.#styles.header)
        )

        return this
    }

    /**
     * Define main styles
     */
    #defineStyles() {
        this.#styles = {
            header: this.#workbook.createStyle({
                font: { bold: true, size: 12 },
                alignment: { horizontal: 'center', vertical: 'center', wrapText: true },
                border: { bottom: { style: 'medium' } },
            }),
            middle: this.#workbook.createStyle({
                alignment: { horizontal: 'center' },
            }),
            float1: this.#workbook.createStyle({
                numberFormat: '0.0',
            }),
            float2: this.#workbook.createStyle({
                numberFormat: '0.00',
            }),
            percent: this.#workbook.createStyle({
                numberFormat: '#0.00%',
            }),
            middleScore: this.#workbook.createStyle({
                fill: {
                    type: 'pattern',
                    patternType: 'solid',
                    fgColor: '#FFF2CC'
                },
            }),
            finalScore: this.#workbook.createStyle({
                fill: {
                    type: 'pattern',
                    patternType: 'solid',
                    fgColor: '#E2EFDA'
                },
            }),
        }

        return this
    }

    /**
     * Define main instances
     */
    #defineWorkbook() {
        this.#workbook = new excel.Workbook({
            defaultFont: { size: 11, name: 'Consolas' },
            author: 'АО "РТЛабс" - tyomka896',
        })

        this.#mainsheet = this.#workbook
            .addWorksheet(`Отчет ${dayjs().format('DD.MM.YYYY')}`)

        return this
    }
}
